
# Gcode Motor Stutter Generator

A gcode generator with serial communication capabilities; specifically made to induce stutter in stepper motors 

Public repository for the software I developed to control the testbench used in experiments in the thesis http://dx.doi.org/10.13140/RG.2.2.17279.69285.


The scripts run locally on a Raspberry Pi 4, and interface with two systems of data acquisition, and one system of control.
- Video acquisition through a compatible camera module.
- Encoder, stepper and load cell data acquisition through serial communication with a separate data collecting unit (Teensy).
- Control through serial communication with a microcontroller running Marlin firmware.


## How to use
1. Add the repository and the subdirectory recording_tools to PATH.
2. Determine the UUID of the USB-connected data collecting unit and the Marlin-run microcontroller. (check 'lsusb' and '/dev/')
3. Replace the pre-filled serial connection path in SerialCapture.py and gcoder.py accordingly.
4. Adjust recording parameters in 'recording_tools/custom_recording_command.txt'.
5. If you use 'libcamera-apps' to record video, change the command in 'recording_tools/recording_trigger.sh' accordingly.
4. Run gcoder.py with your chosen parameters.

For more details, see the thesis. Especially the appendix chapter holds useful information.
