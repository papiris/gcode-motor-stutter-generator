#!/usr/bin/env python3

#---------------------------#

# Copyright (c) 2022 Jacob Dybvald Ludvigsen (jd_lud at pm dot me)

#--------------------------#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3.0
# as published by the Free Software Foundation and appearing in the file
# LICENSE.txt included in the directory of this file.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#--------------------------#

# This script communicates with a serial/USB-connected 3D-printer,
# preferably running Marlin >2.0.x firmware.
# It's capable of:
# -setting and reporting temperature,
# -generating gcode for smooth or stuttering extrusion at different feedrates,
# -running extrusion tests sequentially,
# -maintaining continuous extrusion between runs, 
# 
# This script spawns a child process (serialCapture.py),
# which records serial output from a separate port.
# SerialCapture.py captures and stores the output of a Teensy connected to a separate port.
# The Teensy outputs signals from the extruder stepper, a separate encoder and 4 load cells.
#--------------------------#


from sys import stdout, stdin # show log of child processes and get user input
from curtsies import Input # get keystrokes
import argparse # to accept command-line input
import random # generate jittery numbers
import serial # communicate over serial connection
import time # pause execution when necessary
import pexpect # spawn and communicate with child process
from threading import Thread # read from stdin in a non-blocking way
from queue import Queue, Empty # put elements in stdin and get them when available and convenient
from datetime import datetime # give files descriptive, unique names


### Parse commandline arguments
parser = argparse.ArgumentParser()
parser.add_argument(type=float, nargs = 1, action='store', dest='speed', \
          default='False', help='average feedrate in mm/min')
parser.add_argument(type=float, nargs = 1, action='store', dest='length', \
          default='False', help='total extrusion length')
parser.add_argument('-j', '--jitter', '-s', '--stutter', nargs = "?", action='store', dest='jitter', \
          default='False', const='True', help='whether to cause motor to stutter/jitter')
parser.add_argument('-t', '--temperature',type=int, nargs="?", action='store')
parser.add_argument('-l', '--list', nargs = '*', action = 'store', dest = 'extrusion_list',
          default='False', help='specify different extrusion rates for which to generate gcode commands')
parser.add_argument('-c', '--continuous', nargs='?', action='store', dest='continuous_extrusion', const='True')

args = parser.parse_args()
feedRateNominal, extrusionLength = args.speed, args.length
if args.temperature != "":
    _temperature = args.temperature
if args.extrusion_list != "":
    extrusion_list = args.extrusion_list
    
    

### initialization of child process for comms with logging microcontroller
child = pexpect.spawn('python3 serialCapture.py', timeout=1)
#child.logfile = stdout.buffer
if child.isalive() == True:
    print('\nchild is alive')
    


### non-blocking read of stdin
def enqueue_input(stdin, queue):
    try:
        with Input(keynames='curses') as input_generator:
            for _input in iter(input_generator):
                queue.put(_input)
    except keyboardInterrupt:
        child.sendline('stop_process')
        sys.exit(1)

q=Queue()
t = Thread(target=enqueue_input, args=(stdin, q))
t.daemon = True # thread dies with the program
t.start()



### Initialize printer
def initialization_gcode():
    ser = serial.Serial("/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0",115200)
    time.sleep(1.5)
    ''' Example gcodes for initialization'''
    #sendCommand(ser, "M115\r\n")
    #sendCommand(ser, "M350 E16\r\n")
    #sendCommand(ser, "M18 E\r\n")      #disable stepper to avoid trigger
    #sendCommand(ser, "M154 S5\r\n")    #auto report position every 2 seconds
    sendCommand(ser, "M155 S20\r\n")   #auto report temperature
    sendCommand(ser, "G21\r\n")        #Set mm 
    #sendCommand(ser, "M92 E88.71\r\n") # for reinforced PEEK
    sendCommand(ser, "M92 E89.14\r\n")  # for non-reinforced Apium PEEK
    sendCommand(ser, "G91\r\n") # set relative positioning
    sendCommand(ser, "M302 S0\r\n")
    if _temperature != None:
        tmp_cmd = f"M104 S{_temperature}\r\n"
        sendCommand(ser, tmp_cmd)

        while True:
            try:
                input_key = q.get(timeout=5)
            except Empty:
                print('press n when sufficient temperature is reached')
                input_key = ''
                sendCommand(ser, "M105\r\n")
                pass

            if input_key == 'n':
                print('temperature waiting loop exited')
                break    
    return ser



### send command over serial connection to printer
def sendCommand(ser, command):
    if isinstance(command, list): # if command is a list of strings
        for cmd in command:
            if ser.out_waiting == 0:
                ser.write(str.encode(cmd))
                while True:
                    if ser.in_waiting > 0:
                        line = ser.readline().strip()
                        if line == b'ok':
                            break
                        else:
                            print(line)
                            break
            else:
                time.sleep(0.5) # wait a little until the write buffer clears
                     

    else: # if command is a single string
        if ser.out_waiting == 0:
            ser.write(str.encode(command))
            while True:
                if ser.in_waiting > 0:
                    line = ser.readline().strip()
                    print(line)
                    # wait for confirmation from printer
                    if line == b'ok':
                        break
                    ack = line.find(b'ok') # for edge cases when ok is part of a longer message
                    if ack > -1:
                        break
        else:
            time.sleep(0.5) # wait a little until the write buffer clears



### maintains constant extrusion until the defined "input_key" is pressed.
def maintain_extrusion(ser):
    child.sendline('0') #reset teensy to zero values
    time.sleep(1)
    input_key=''
    feedrate = 20 #mm/min
    increment = 3 #mm
    cmd = f"G1 E{increment} F{feedrate}\r\n"

    while True:
        try:
            input_key = q.get(timeout=60/(feedrate/increment))
        except Empty:
            print(f'printing continuously at feedrate {feedrate} mm/min. press n to stop')
            sendCommand(ser, cmd)
            pass
        
        else:
            if input_key == 'n':
                print('extrusion loop stopped, moving on')
                child.sendline('0') #reset teensy to zero values
                time.sleep(1.5) # wait for teensy to reset so we avoid race condition
                break



### Create a list of random numbers distributed around desired feedrate.
### Emulates a defect/stuttering extruder
def randomNum(feedRate, extrusionLengthRandom, duration):
    extrusion_time = 0
    feedrateList=[]
    increment = 0.3 #mm
    # since we only accept values above 0, the stuttery extrusion will be skewed to faster feedrates.
    # We extrude until {duration} time of extrusion is reached.
    while extrusion_time < duration:
        feedrate = random.gauss(float(feedRate), 70)
        if feedrate <= 0:
            continue
        feedrateList.append(feedrate)
        extrusion_time += (increment/feedrate)
    
    return feedrateList, increment
    


### Make Gcode commands from feedrate and increment
def printCommands(feedrateList, increment):
    commandList = []
    if isinstance(feedrateList, list): # list generated by randomNum()
        for feedrate in feedrateList:
            cmd = f"G1 E{increment} F{feedrate}\r\n"
            commandList.append(cmd)
    
    else:
        cmd = f"G1 E{increment} F{feedrateList}\r\n"
        commandList.append(cmd)
    
    return commandList


### Main function calls on other functions as defined
def main():
    duration  = 30 #seconds
    feedrateList = []
    ser = initialization_gcode()
   
    if args.extrusion_list != 'False':
        for feedRateNominal in extrusion_list:
                maintain_extrusion(ser)
                
                extrusionLength = float(feedRateNominal) * (duration / 60) # setting extrusion to ca 30 seconds 
                feedRateStutterList, increment = randomNum(feedRateNominal, extrusionLength, duration) # generating a stutterlist for the defined feedrate
                smooth_cmd = printCommands(feedRateNominal, extrusionLength)
                stutter_cmd = printCommands(feedRateStutterList, increment)
                
                # run raspivid with predefined settings and descriptive filename
                _ct = datetime.now()
                pexpect.run(f'recording_trigger.sh {duration*1000} {_ct.month}.{_ct.day}_{_ct.hour}:{_ct.minute}_{feedRateNominal}mm_min_smooth -p')
                time.sleep(1.5) # wait for raspivid to initialize
                
                print(f'Printing smoothly at feedrate {feedRateNominal} mm/min for {duration} seconds')
                sendCommand(ser, smooth_cmd)
                time.sleep(duration)
                child.sendline(f'{feedRateNominal}mm_min_smooth')
                
                maintain_extrusion(ser)
                
                # run raspivid with predefined settings and descriptive filename
                _ct = datetime.now()
                pexpect.run(f'recording_trigger.sh {duration*1000} {_ct.month}.{_ct.day}_{_ct.hour}:{_ct.minute}_{feedRateNominal}mm_min_stutter -p')
                time.sleep(1.5)# wait for raspivid to initialize
                
                print(f'Printing jittery at feedrate {feedRateNominal} mm/min for {duration} seconds')
                pre_command_time = time.time()
                sendCommand(ser, stutter_cmd)
                # Since short segments need processing time, sendCommand() doesn't immediately return.
                # Therefore we don't sleep as long as set duration.
                while time.time() - pre_command_time < duration:
                    time.sleep(1)
                child.sendline(f'{feedRateNominal}mm_min_stutter')
                
    else:
        feedRateNominal = args.speed[0]
        extrusionLength = args.length[0]
    
        if args.jitter == 'True': # stuttering extrusion
            feedrateList, increment = randomNum(feedRateNominal, extrusionLength)
            cmd = printCommands(feedrateList, increment)
            sendCommand(ser, cmd)
            
        else: # Normal single-speed extrusion
            feedrateList.append(feedRateNominal)
            increment = extrusionLength
            cmd = printCommands(feedrateList, increment)
            sendCommand(ser, cmd)
    
    if args.continuous_extrusion == 'True': # Keep extruding after last run
        maintain_extrusion(ser)
        
    # Turn off heater and extrude a little more to avoid charring filament
    sendCommand(ser, "M104 S0\r\n")
    if _temperature != None:
        if _temperature > 50:
            sendCommand(ser, "G1 E8 F20\r\n")
    
    # tell child process to exit on its own
    child.sendline('stop_process')
    time.sleep(2)
    if child.isalive() == True:
        print('child is still alive after being told to exit')
    child.terminate(force=True)
    time.sleep(3)
    if child.isalive() == True:
        print('child is still alive after being terminated')
    else:
        print('child exited disgracefully')
    
    
    
if __name__ == "__main__":
    main()
