#!/bin/env python3


#--------------------------#

# Copyright (c) 2022 Jacob Dybvald Ludvigsen (jd_lud at pm dot me)

#--------------------------#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3.0
# as published by the Free Software Foundation and appearing in the file
# LICENSE.txt included in the directory of this file.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#--------------------------#
# This script communicates with a microcontroller (Teensy).
# The MCU monitors the position of a stepper motor (filament extruder),
# and an encoder placed after the bowden tube.
# It also monitors four load cells placed appropriately on the printer.
# The script reads the MCU's serial output,
# and saves the values as csv files with names sent by the parent process.
# When the parent signals it, this script sends "0" to reset MCU.


from sys import stdin, stdout, exit
import serial
import time
from datetime import datetime
import csv
from select import select

# Initialize serial port
port = "/dev/serial/by-id/usb-Teensyduino_USB_Serial_7855130-if00"
ser = serial.Serial(port, baudrate=9600, timeout=0.2)
ser.close()
ser.open()
time.sleep(2)


### Function to read or write to serial port
def serial_comms(reset_flag=0):
    
    if reset_flag == 1:
        # Write to serial telling device to reset, empty input buffer
        ser.write(bytes('0', 'utf-8'))
        ser.reset_input_buffer()
        print('Teensy reset')
        time.sleep(1.5) # Teensy has undefined behaviour (c; throws a fit) if it doesn't get some time to process after a write.
        
    else:
        # Read everything from serial input buffer, convert from bytes to float, put in list, return
        cleaned_lines = []
        _buffer = ser.readlines(ser.in_waiting)
        for line in _buffer:
            cleaned_line = []
            chunks = line.split()
            for chunk in chunks:
                chunk = float(chunk)
                cleaned_line.append(chunk)
                
            cleaned_lines.append(cleaned_line)
        
        return cleaned_lines




### function to accept stdin
def parent_comms():
    data = ""
    
    rfds, wfds, efds = select( [stdin], [], [], 0.2) # check if there's anything available to read on stdin
    if rfds:
        data = stdin.readline().strip()
        stdin.flush()
        stdout.flush()
    
    return data


### function to generate files
def filewriter(data, lines):
    _ct = datetime.now()
    with open(f'{_ct.month}.{_ct.day}_{_ct.hour}:{_ct.minute}_{data}_encoder.csv', 'w') as fn: 
                  
        writer = csv.writer(fn)
        writer.writerows(lines)
        print('writing data')
        fn.close()


def main():
    csv_lines = []
    while ser.is_open:
        data = parent_comms()
        
        if data == '0': # parent proc signals to reset mcu
            serial_comms(1)
            csv_lines = []
            continue
        
        elif data == "stop_process":
            ser.close()
            time.sleep(1)
            exit()
            break
            
        elif data == "":
            k = 0
            serial_lines = serial_comms()
            for line in serial_lines:
                if line != []:
                    if k < 10 and line[0] > 50: # filter out lines from before MCU reset
                        k += 1
                        continue
                    k += 1
                    csv_lines.append(line)
        
        else: # data contains a filename
            filewriter(data, csv_lines)
            csv_lines = []
            
            
if __name__ == "__main__":
    main()
